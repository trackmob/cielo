# frozen_string_literal: true
module Cielo
  module Version
    MAJOR = 1
    MINOR = 1
    PATCH = 1
    STRING = "#{MAJOR}.#{MINOR}.#{PATCH}".freeze
  end
end
